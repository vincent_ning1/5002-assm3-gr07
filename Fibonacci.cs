using System;

namespace _5002_assm3_gr07
{
    class Fibonacci
    {
        public Fibonacci()
        {
            // do nothing
        }

        public string strFiboacciSequence(int terms)
        {
            int oldNum = 0;
            int curNum = 1;
            int newNum;
            string strSequence = String.Empty;

            for (int i = 0; i < terms; i++)
            {
                newNum = oldNum + curNum;
                oldNum = curNum;
                curNum = newNum;

                strSequence += newNum.ToString();
                
                if(i < (terms-1))
                {
                    strSequence += ", ";
                }
                else { strSequence += "."; }
            }

            return strSequence;
        }
    }
}
