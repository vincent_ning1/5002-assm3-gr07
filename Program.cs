﻿using System;
using System.Collections.Generic;   // by Sing : For question3

namespace _5002_assm3_gr07
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            var selection = 0;

            do {
                Console.Clear();
                Console.WriteLine("******************************************************************");
                Console.WriteLine("*****                         Menu                           *****");
                Console.WriteLine("******************************************************************");
                Console.WriteLine("*****                                                        *****");
                Console.WriteLine("*****             Option 1:leap years - Vincent              *****");
                Console.WriteLine("*****             Option 2:Woking hours - Kaylene            *****");
                Console.WriteLine("*****             Option 3:students & teachers list - Sing   *****");
                Console.WriteLine("*****             Option 4:Fibonacci sequence - Sachet       *****");
                Console.WriteLine("*****             Option 5:Exit the program                  *****");
                Console.WriteLine("*****                                                        *****");
                Console.WriteLine("******************************************************************");
                Console.WriteLine();
                Console.WriteLine("Choose a number:");
                var t = Console.ReadLine();
                bool checkInput = int.TryParse(t, out selection);
                
                if (checkInput)
                {
                    switch (selection)
                    {

                        case 1:
                            Console.Clear();
                            listOfLeapYears();
                            Console.WriteLine("Press <Enter> to go to the menu");
                            Console.ReadKey();
                            break;
                        case 2:
                            Console.Clear();
                            workingHours();
                            Console.WriteLine("Press <Enter> to go to the menu");
                            Console.ReadKey();
                            break;

                        // by Sing : I added this code for question3.
                        case 3:
                            Console.Clear();
                            listOfStudents();

                            Console.WriteLine("Press <Enter> to go to the menu");
                            Console.ReadKey();
                            break;

                         case 4:
                            Console.Clear();
                            printFibonacciSequence();

                            Console.WriteLine("Press <Enter> to go to the menu");
                            Console.ReadKey();
                            break;
                        case 5:
                            Console.Clear();
                            Console.WriteLine("Thanks for using the program");
                            break;
                        default:
                            Console.WriteLine($"You selected option {selection}");
                            Console.WriteLine("Press <Enter> to quit the program"); 
                            Console.ReadKey();
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("You didn't type in a number");
                    selection = 0;
                    Console.WriteLine("Press <Enter> to try again");
                    Console.ReadKey();
                }

                Console.WriteLine();

            }while(selection < 5);

            
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }



        public static void workingHours()
        {
           Console.WriteLine("How many days do you work in a week? (enter a number between 1-7) "); 
           var days =  Convert.ToInt32(Console.ReadLine());
           if (days < 7)
           {
               Console.WriteLine("Thank you for your input, you work " + days + " days a week");
           }
           else if (days > 7) 
           {
                Console.WriteLine("Please enter enter a number between 1 and 7");
                Console.WriteLine("How many days do you work in a week? (enter a number between 1-7) "); 
            days =  Convert.ToInt32(Console.ReadLine());
           }

           Console.WriteLine ("How many hours a day do you work?");
           var hours = Convert.ToInt32(Console.ReadLine());
           var totalHours = hours * days;
           var part = 40 - totalHours;
           var over = totalHours - 40;
           if (hours > 24)
           {
               Console.WriteLine ("please enter a number between 1 and 24. There are only 24 hours in a day");
               Console.WriteLine ("How many hours a day do you work ?");
               hours = Convert.ToInt32(Console.ReadLine());
           }
           else if (totalHours == 40)
           {
                Console.WriteLine ("You work full time");
           } 
           else if (totalHours < 40)
           {
            Console.WriteLine($"You are working " + part + " less hours than 40 hours a week");
           }
           else if (totalHours > 40)
           {
            Console.WriteLine($"You are working " + over + " more hours than 40 hours a week");
           }
           
           var yearlyHours = totalHours * 48;
           Console.WriteLine("In total you work " + totalHours +" hours a week");
           Console.WriteLine("Therefore you work " + yearlyHours + " hours a year, excluding 4 weeks holiday");
        }

         static void listOfLeapYears()
        {
            int endYear = 2067;
            
            
            Console.WriteLine("The leap years between 2017 to 2067 are:");

            for (int year = 2017; year <= endYear; year++)
            {
                if (year % 4 == 0 )
                {
                    if (year % 100 ==0)
                    {
                        if(year % 400 ==0)
                        {
                            Console.WriteLine(year);
                        }
                    }
                    else
                    {
                        Console.WriteLine(year);
                    }
                }
            }   
        }


        static void listOfStudents()
        {
            // Key   : Students & teachers' name
            // Value : 'S' for students, 'T' for teachers
            Dictionary<string, char> people = createListOfPeople();

            // Display the number of students & of teachers
            Console.WriteLine("**********************************");
            Console.WriteLine("            Information");
            Console.WriteLine("**********************************");
            Console.WriteLine(" The number of students : " + countStudents(people));
            Console.WriteLine(" The number of teachers : " + countTeachers(people));

            // Display names of students
            Console.WriteLine();
            Console.WriteLine(" List of students : ");
            Console.WriteLine(" --------------------------------");
            Console.Write(printStudentsNames(people));
            Console.WriteLine("**********************************");
        }

        /*
         * This method creat a dicionary for teachers and students.
         * In this moment, it just is hard coded.
         * In the future, you can change the way of adding data by reading file, querying database and so on.
         */
        static Dictionary<string, char> createListOfPeople()
        {
            var people = new Dictionary<string, char>();

            people.Add("Sing", 'S');
            people.Add("Vincent", 'S');
            people.Add("Jeff", 'T');
            people.Add("Stefan", 'T');
            people.Add("Murrey", 'T');
            people.Add("Ray", 'T');
            people.Add("Dani", 'S');
            people.Add("Kalyer", 'S');
            people.Add("Brett", 'S');
            people.Add("John", 'S');
            people.Add("Matt", 'S');
            people.Add("Josp", 'S');
            people.Add("Phoebe", 'S');
            people.Add("Jordan", 'S');
            people.Add("Mobee", 'S');
            people.Add("Travis", 'S');
            people.Add("Tina", 'S');
            people.Add("Rab", 'S');
            people.Add("Nathon", 'S');
            people.Add("Marisa", 'S');
            people.Add("Karan", 'S');
            people.Add("Ethan", 'S');

            return people;
        }

        /* 
         * This method return the number of students or of teachers or of all.
         * Use the second argument 'option' to choose one group of them; 'S' for students, 'T' for teachers.
         * If you do not use the argument 'option', this method will return the number of all people.
         * Related methods : countStudents(), countTeachers()
         */
        static int countPeople(Dictionary<string, char> people, char option = 'A')
        {
            if(option == 'A') return people.Count;

            int cnt = 0;

            foreach(var person in people)
            {
                if(person.Value == option){ ++cnt; }
            }

            return cnt;
        }

        static int countStudents(Dictionary<string, char> people){ return countPeople(people, 'S'); }
        static int countTeachers(Dictionary<string, char> people){ return countPeople(people, 'T'); }

        /*
         * This method collect students' name from dictionary 'people'
         * and return a string of the student's name collected.
         */
        static string printStudentsNames(Dictionary<string, char> people)
        {
            string listOfStudentsNames = string.Empty;
            
            foreach(var person in people)
            {
                if(person.Value == 'S')
                {
                    listOfStudentsNames += ' ' + person.Key + "\n";
                }
            }

            return listOfStudentsNames;

        }

        public static void printFibonacciSequence()
        {
            var fibo = new Fibonacci();
            int terms;

            do
            {
            
                Console.Write("How many Fibonacci terms do you want ? ");
                bool isInt = int.TryParse(Console.ReadLine(), out terms);
                Console.WriteLine();
            
                if(isInt == true)
                {    
                    Console.WriteLine("This is Your answer");
                    Console.WriteLine(fibo.strFiboacciSequence(terms));
                    break;
                }
                else
                {
                    Console.WriteLine("Please re-enter a number as terms ! (Press enter to continue)");  
                    Console.ReadKey();

                    Console.Clear();
                    Console.WriteLine("Start : ");
                }

            } while(true);                
            Console.WriteLine();
        }
    }
}
